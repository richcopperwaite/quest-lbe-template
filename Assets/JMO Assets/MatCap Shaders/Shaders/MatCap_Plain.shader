// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// MatCap Shader, (c) 2015-2019 Jean Moreno

Shader "MatCap/Vertex/Plain"
{
	Properties
	{
		_Color ("Main Color", Color) = (0.5,0.5,0.5,1)
		_MatCap ("MatCap (RGB)", 2D) = "white" {}
	}
	
	Subshader
	{
		Tags { "RenderType"="Opaque" }
		
		Pass
		{
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest
				#pragma multi_compile_fog
				#include "UnityCG.cginc"
				
				struct v2f
				{
					float4 pos	: SV_POSITION;
					float2 cap	: TEXCOORD0;
					UNITY_FOG_COORDS(1)
				};
				
				v2f vert (appdata_base v)
				{
					v2f o;
					o.pos = UnityObjectToClipPos(v.vertex);

					float3 worldPos = mul(unity_ObjectToWorld, float4(v.vertex.xyz, 1.0f)).xyz;
					float3 worldNorm = normalize(mul(unity_ObjectToWorld, float4(v.normal.xyz, 0.0f)).xyz);

					float3x3 viewMatrix = (float3x3)UNITY_MATRIX_V;
					viewMatrix[2] = normalize(worldPos - _WorldSpaceCameraPos.xyz);
					viewMatrix[0] = normalize(cross(float3(0, 1, 0), viewMatrix[2]));
					//viewMatrix[0] = cross(viewMatrix[1], viewMatrix[2]);
					viewMatrix[1] = cross(viewMatrix[2], viewMatrix[0]);
						
					worldNorm = mul(viewMatrix, worldNorm);
					o.cap.xy = worldNorm.xy * 0.5 + 0.5;

					UNITY_TRANSFER_FOG(o, o.pos);

					return o;
				}
				
				uniform float4 _Color;
				uniform sampler2D _MatCap;

				float4 frag (v2f i) : COLOR
				{
					float4 mc = tex2D(_MatCap, i.cap);
					mc = _Color * mc;
					UNITY_APPLY_FOG(i.fogCoord, mc);
					return mc;
				}
			ENDCG
		}
	}
	
	Fallback "VertexLit"
}
