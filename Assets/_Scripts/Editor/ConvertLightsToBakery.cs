﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.SceneManagement;

public class ConvertLightsToBakery
{
    static SerializedProperty ftraceLightColor;
    static SerializedProperty ftraceLightIntensity;
    static SerializedProperty ftraceLightShadowSpread;
    static SerializedProperty ftraceLightSamples;
    static SerializedProperty ftraceLightBitmask;
    static SerializedProperty ftraceLightBakeToIndirect;
    static SerializedProperty ftraceLightShadowmask;
    static SerializedProperty ftraceLightShadowmaskDenoise;
    static SerializedProperty ftraceLightIndirectIntensity;
    static SerializedProperty ftraceLightTexture, ftraceLightCSTilingX, ftraceLightCSTilingY, ftraceLightCSOffsetX, ftraceLightCSOffsetY;
    static SerializedProperty ftraceLightCutoff;
    static SerializedProperty ftraceLightProj;
    static SerializedProperty ftraceLightTexture2D;
    static SerializedProperty ftraceLightAngle;
    static SerializedProperty ftraceLightIES;
    static SerializedProperty ftraceLightRealisticFalloff;
    static SerializedProperty ftraceLightFalloffMinRadius;
    static SerializedProperty ftraceLightInnerAngle;

    static bool isHDRP = false;
    static bool isLWRP = false;

    static void InitDirectSerializedProperties(SerializedObject obj)
    {
        ftraceLightColor = obj.FindProperty("color");
        ftraceLightIntensity = obj.FindProperty("intensity");
        ftraceLightIndirectIntensity = obj.FindProperty("indirectIntensity");
        ftraceLightShadowSpread = obj.FindProperty("shadowSpread");
        ftraceLightSamples = obj.FindProperty("samples");
        ftraceLightBitmask = obj.FindProperty("bitmask");
        ftraceLightBakeToIndirect = obj.FindProperty("bakeToIndirect");
        ftraceLightShadowmask = obj.FindProperty("shadowmask");
        ftraceLightShadowmaskDenoise = obj.FindProperty("shadowmaskDenoise");
        ftraceLightTexture = obj.FindProperty("cloudShadow");
        ftraceLightCSTilingX = obj.FindProperty("cloudShadowTilingX");
        ftraceLightCSTilingY = obj.FindProperty("cloudShadowTilingY");
        ftraceLightCSOffsetX = obj.FindProperty("cloudShadowOffsetX");
        ftraceLightCSOffsetY = obj.FindProperty("cloudShadowOffsetY");

        isHDRP = (obj.targetObject as BakeryDirectLight).GetComponent("HDAdditionalLightData") != null;
    }

    static void InitPointSerializedProperties(SerializedObject obj)
    {
        ftraceLightColor = obj.FindProperty("color");
        ftraceLightIntensity = obj.FindProperty("intensity");
        ftraceLightIndirectIntensity = obj.FindProperty("indirectIntensity");
        ftraceLightShadowSpread = obj.FindProperty("shadowSpread");
        ftraceLightCutoff = obj.FindProperty("cutoff");
        ftraceLightAngle = obj.FindProperty("angle");
        ftraceLightInnerAngle = obj.FindProperty("innerAngle");
        ftraceLightSamples = obj.FindProperty("samples");
        ftraceLightProj = obj.FindProperty("projMode");
        ftraceLightTexture = obj.FindProperty("cubemap");
        ftraceLightTexture2D = obj.FindProperty("cookie");
        ftraceLightIES = obj.FindProperty("iesFile");
        ftraceLightBitmask = obj.FindProperty("bitmask");
        ftraceLightBakeToIndirect = obj.FindProperty("bakeToIndirect");
        ftraceLightRealisticFalloff = obj.FindProperty("realisticFalloff");
        ftraceLightShadowmask = obj.FindProperty("shadowmask");
        ftraceLightFalloffMinRadius = obj.FindProperty("falloffMinRadius");

        var hdrpLight = (obj.targetObject as BakeryPointLight).GetComponent("HDAdditionalLightData");
        isHDRP = hdrpLight != null;

#if UNITY_2018_1_OR_NEWER

#if UNITY_2019_3_OR_NEWER
        var rpipe = GraphicsSettings.currentRenderPipeline;
#else
        var rpipe = GraphicsSettings.renderPipelineAsset;
#endif

        if (rpipe != null && (rpipe.GetType().Name.StartsWith("Lightweight") || rpipe.GetType().Name.StartsWith("Universal")))
        {
            isLWRP = true;
        }
#endif
    }

    static void GetLinearLightParameters(Light light, out float lightR, out float lightG, out float lightB, out float lightInt)
    {
        if (PlayerSettings.colorSpace != ColorSpace.Linear)
        {
            lightInt = light.intensity;
            lightR = light.color.r;
            lightG = light.color.g;
            lightB = light.color.b;
            return;
        }

        if (!GraphicsSettings.lightsUseLinearIntensity)
        {
            lightR = Mathf.Pow(light.color.r * light.intensity, 2.2f);
            lightG = Mathf.Pow(light.color.g * light.intensity, 2.2f);
            lightB = Mathf.Pow(light.color.b * light.intensity, 2.2f);
            lightInt = Mathf.Max(Mathf.Max(lightR, lightG), lightB);
            lightR /= lightInt;
            lightG /= lightInt;
            lightB /= lightInt;
        }
        else
        {
            lightInt = light.intensity;
            lightR = light.color.linear.r;
            lightG = light.color.linear.g;
            lightB = light.color.linear.b;
        }
    }

    static void MatchToLWRPLight(Light l)
    {
        ftraceLightRealisticFalloff.boolValue = true;
        ftraceLightFalloffMinRadius.floatValue = 0.01f;
        if (l.type == LightType.Spot)
        {
            ftraceLightProj.intValue = (int)BakeryPointLight.ftLightProjectionMode.Cone;

            var so = new SerializedObject(l);
            if (so == null) return;

            SerializedProperty lightInnerAngle = so.FindProperty("m_InnerSpotAngle");
            if (lightInnerAngle == null) return;
            ftraceLightInnerAngle.floatValue = (lightInnerAngle.floatValue / ftraceLightAngle.floatValue) * 100;
        }
    }

    static void MatchToHDRPLight(Light l)
    {
        ftraceLightRealisticFalloff.boolValue = true;
        ftraceLightFalloffMinRadius.floatValue = 0.01f;

        ftraceLightIntensity.floatValue /= Mathf.PI;

        var hdrpLight = l.GetComponent("HDAdditionalLightData");
        if (hdrpLight == null) return;

        var so = new SerializedObject(hdrpLight);
        if (so == null) return;

        SerializedProperty hdrpLightTypeExtent = so.FindProperty("m_PointlightHDType");
        if (hdrpLightTypeExtent == null) return;

        int extendedLightType = hdrpLightTypeExtent.intValue;
        if (extendedLightType != 0) return;

        if (l.type == LightType.Spot)
        {
            SerializedProperty hdrpLightSpotShape = so.FindProperty("m_SpotLightShape");
            if (hdrpLightSpotShape == null) return;

            int spotShape = hdrpLightSpotShape.intValue;
            if (spotShape != 0) return;

            ftraceLightProj.intValue = (int)BakeryPointLight.ftLightProjectionMode.Cone;
        }

        SerializedProperty hdrpLightInnerAngle = so.FindProperty("m_InnerSpotPercent");
        if (hdrpLightInnerAngle == null) return;
        ftraceLightInnerAngle.floatValue = hdrpLightInnerAngle.floatValue;
    }

    [MenuItem("Bakery/Utilities/Convert Lights")]
    public static void ConvertLights()
    {
        Light[] allLights = Resources.FindObjectsOfTypeAll<Light>();

        var bakeryRuntimePath = ftLightmaps.GetRuntimePath();
        UnityEngine.Object spotCookieTexture = AssetDatabase.LoadAssetAtPath(bakeryRuntimePath + "ftUnitySpotTexture.bmp", typeof(Texture2D));

        foreach (Light light in allLights)
        {
            GameObject parentObject = light.gameObject;
            if (parentObject.scene != SceneManager.GetActiveScene())
            {
                continue;
            }

            switch (light.type)
            {
                case LightType.Directional:
                    {
                        BakeryDirectLight bakeryLight = parentObject.GetComponent<BakeryDirectLight>();
                        if (bakeryLight == null) bakeryLight = parentObject.AddComponent<BakeryDirectLight>();
                        if (light == null) continue;


                        //if (!light.enabled) continue;
                        var so = new SerializedObject(bakeryLight);

                        InitDirectSerializedProperties(so);

                        if (PlayerSettings.colorSpace != ColorSpace.Linear)
                        {
                            ftraceLightColor.colorValue = light.color;
                            ftraceLightIntensity.floatValue = light.intensity;
                        }
                        else if (!GraphicsSettings.lightsUseLinearIntensity)
                        {
                            float lightR, lightG, lightB, lightInt;
                            GetLinearLightParameters(light, out lightR, out lightG, out lightB, out lightInt);
                            ftraceLightColor.colorValue = new Color(lightR, lightG, lightB);
                            ftraceLightIntensity.floatValue = lightInt;
                        }
                        else
                        {
                            ftraceLightColor.colorValue = light.color;
                            ftraceLightIntensity.floatValue = light.intensity;
                        }
                        ftraceLightIndirectIntensity.floatValue = light.bounceIntensity;
                        if (isHDRP) ftraceLightIntensity.floatValue /= Mathf.PI;

                        so.ApplyModifiedProperties();
                    }
                    break;
                case LightType.Point:
                case LightType.Spot:
                    {
                        BakeryPointLight bakeryLight = parentObject.GetComponent<BakeryPointLight>();
                        if (bakeryLight == null) bakeryLight = parentObject.AddComponent<BakeryPointLight>();
                        if (light == null) continue;
                        //if (!light.enabled) continue;
                        var so = new SerializedObject(bakeryLight);
                        InitPointSerializedProperties(so);

                        if (PlayerSettings.colorSpace != ColorSpace.Linear)
                        {
                            ftraceLightColor.colorValue = light.color;
                            ftraceLightIntensity.floatValue = light.intensity;
                        }
                        else if (!GraphicsSettings.lightsUseLinearIntensity)
                        {
                            float lightR, lightG, lightB, lightInt;
                            GetLinearLightParameters(light, out lightR, out lightG, out lightB, out lightInt);
                            ftraceLightColor.colorValue = new Color(lightR, lightG, lightB);
                            ftraceLightIntensity.floatValue = lightInt;
                        }
                        else
                        {
                            ftraceLightColor.colorValue = light.color;
                            ftraceLightIntensity.floatValue = light.intensity;
                        }
                        ftraceLightCutoff.floatValue = light.range;
                        ftraceLightAngle.floatValue = light.spotAngle;

                        if (light.type == LightType.Point)
                        {
                            if (light.cookie == null)
                            {
                                ftraceLightProj.enumValueIndex = (int)BakeryPointLight.ftLightProjectionMode.Omni;
                                ftraceLightTexture.objectReferenceValue = null;
                            }
                            else
                            {
                                ftraceLightProj.enumValueIndex = (int)BakeryPointLight.ftLightProjectionMode.Cubemap;
                                ftraceLightTexture.objectReferenceValue = light.cookie;
                            }
                        }
                        else if (light.type == LightType.Spot)
                        {
                            ftraceLightProj.enumValueIndex = (int)BakeryPointLight.ftLightProjectionMode.Cookie;
                            if (light.cookie == null)
                            {
                                ftraceLightTexture2D.objectReferenceValue = spotCookieTexture;
                            }
                            else
                            {
                                ftraceLightTexture2D.objectReferenceValue = light.cookie;
                            }
                        }
                        ftraceLightIndirectIntensity.floatValue = light.bounceIntensity;

                        if (isHDRP) MatchToHDRPLight(light);
                        if (isLWRP) MatchToLWRPLight(light);

                        so.ApplyModifiedProperties();
                    }
                    break;
            }

            light.enabled = false;
        }
    }
}
