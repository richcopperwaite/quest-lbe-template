﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TSMCalibration : MonoBehaviour
{
    // The set of object layers to show while calibrating.
    // Make sure all objects that you wish to be VISIBLE during
    // the calibration process are included in these layers.
    public LayerMask ShowWhileCalibrating;

    // The set of object layers to hide while calibrating
    // Make sure all objects you wish to be INVISIBLE during
    // the calibration process are included in these layers.
    public LayerMask HideWhileCalibrating;

    public GameObject ClickedPointPrefab;

    public Transform RightController;
    public Transform ObjectToOffset;

    public TextMesh TextMeshDisplay;

    private bool _isCalibrating = false;
    private bool _isSettingPoint1 = true;

    private GameObject _clickedPoint1;
    private GameObject _clickedPoint2;

    private GameObject ControlPoint1;
    private GameObject ControlPoint2;

    private Vector3 MeasuredPoint1;
    private Vector3 MeasuredPoint2;

    private void BeginCalibration()
    {
        Camera mainCamera = Camera.main;
        if (mainCamera != null)
        {
            // add showwhilecalibrating, remove hidewhilecalibrating

            mainCamera.cullingMask |= ShowWhileCalibrating.value;
            mainCamera.cullingMask &= ~HideWhileCalibrating.value;
        }

        _isCalibrating = true;
        _isSettingPoint1 = true;

        GameObject[] ControlPoints1 = GameObject.FindGameObjectsWithTag("ControlPoint1");
        GameObject[] ControlPoints2 = GameObject.FindGameObjectsWithTag("ControlPoint2");

        if(ControlPoints1.Length == 0)
        {
            TextMeshDisplay.text = "Error: Both calibration control points must be placed in scene";
            enabled = false;
            return;
        }

        if(ControlPoints1.Length > 1 || ControlPoints2.Length > 1)
        {
            TextMeshDisplay.text = "Error: Duplicate calibration control points have been placed in scene";
            enabled = false;
            return;
        }

        ControlPoint1 = ControlPoints1[0];
        ControlPoint2 = ControlPoints2[0];

        ControlPoint1.SetActive(true);
        ControlPoint2.SetActive(false);

        TextMeshDisplay.text = "Move right controller to corresponding\npoint in real world and pull trigger";
    }

    private void ApplyCalibration()
    {
        Vector3 ControlPos1 = ControlPoint1.transform.position;
        Vector3 ControlPos2 = ControlPoint2.transform.position;

        Vector3 RelativePoint1 = ObjectToOffset.InverseTransformPoint(MeasuredPoint1);
        Vector3 RelativePoint2 = ObjectToOffset.InverseTransformPoint(MeasuredPoint2);

        Vector3 WorldOffset1 = MeasuredPoint1 - ControlPos1;
        Vector3 WorldOfsset2 = MeasuredPoint2 - ControlPos2;

        float ControlAngle = Mathf.Atan2(ControlPos1.z - ControlPos2.z, ControlPos1.x - ControlPos2.x);
        float Angle = Mathf.Atan2(MeasuredPoint1.z - MeasuredPoint2.z, MeasuredPoint1.x - MeasuredPoint2.x);

        float AngleOffset = Angle - ControlAngle;
        ObjectToOffset.rotation *= Quaternion.Euler(0.0f, Mathf.Rad2Deg * AngleOffset, 0.0f);

        Vector3 RotatedPoint1 = ObjectToOffset.TransformPoint(RelativePoint1);
        Vector3 RotatedPoint2 = ObjectToOffset.TransformPoint(RelativePoint2);

        Vector3 ControlCenter = Vector3.Lerp(ControlPos1, ControlPos2, 0.5f);
        Vector3 Center = Vector3.Lerp(RotatedPoint1, RotatedPoint2, 0.5f);

        Vector3 Offset = ControlCenter - Center;
        Vector3 XZOffset = new Vector3(Offset.x, 0.0f, Offset.z);

        ObjectToOffset.position += XZOffset;
    }

    private void OnSetPoint1()
    {
        MeasuredPoint1 = RightController.position;
        _clickedPoint1 = Instantiate(ClickedPointPrefab, MeasuredPoint1, Quaternion.identity);

        _isSettingPoint1 = false;
        ControlPoint1.SetActive(false);
        ControlPoint2.SetActive(true);
    }

    private void OnSetPoint2()
    {
        MeasuredPoint2 = RightController.position;
        _clickedPoint2 = Instantiate(ClickedPointPrefab, MeasuredPoint2, Quaternion.identity);

        ApplyCalibration();

        Camera mainCamera = Camera.main;
        if (mainCamera != null)
        {
            // add hidewhilecalibrating, remove showwhilecalibrating

            mainCamera.cullingMask |= HideWhileCalibrating.value;
            mainCamera.cullingMask &= ~ShowWhileCalibrating.value;
        }

        _isCalibrating = false;
        ControlPoint1?.SetActive(false);
        ControlPoint2?.SetActive(false);
        _clickedPoint1?.SetActive(false);
        _clickedPoint2?.SetActive(false);
    }

    // Start is called before the first frame update
    void Start()
    {
        //MeasuredPoint1 = ClickedPoint1.transform.position;
        //MeasuredPoint2 = ClickedPoint2.transform.position;

        //ApplyCalibration();

        //MeasuredPoint1 = ClickedPoint1.transform.position;
        //MeasuredPoint2 = ClickedPoint2.transform.position;

        //ApplyCalibration();

        BeginCalibration();
    }

    // Update is called once per frame
    void Update()
    {
        if (_isCalibrating)
        {
            if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.RTouch))
            {
                if (_isSettingPoint1)
                {
                    OnSetPoint1();
                }
                else
                {
                    OnSetPoint2();
                }
            }
        }
        else
        {
            if(OVRInput.GetDown(OVRInput.Button.Start, OVRInput.Controller.All))
            {
                BeginCalibration();
            }
        }
    }
}
