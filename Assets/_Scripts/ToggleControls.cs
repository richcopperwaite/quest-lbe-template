﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(OVRPlayerController))]
public class ToggleControls : MonoBehaviour
{
    private OVRPlayerController PlayerController;

    public GameObject[] GameObjectsToToggle;

    public float HoldDuration = 10.0f;

    private float LastDown = float.MaxValue;
    private bool IsStuffEnabled = false;

    // Start is called before the first frame update
    void Start()
    {
        PlayerController = GetComponent<OVRPlayerController>();

        Toggle(IsStuffEnabled);
    }

    void Toggle(bool newValue)
    {
        PlayerController.EnableLinearMovement = newValue;
        PlayerController.EnableRotation = newValue;

        foreach (GameObject go in GameObjectsToToggle)
        {
            go.SetActive(newValue);
        }
    }

    // Update is called once per frame
    void Update()
    {
        //bool IsHeld = OVRInput.Get(OVRInput.Button.Start, OVRInput.Controller.All);

        //if(!IsHeld)
        //{
        //    LastDown = float.MaxValue;
        //}
        //else if(LastDown == float.MaxValue)
        //{
        //    LastDown = Time.timeSinceLevelLoad;
        //}

        //if(Time.timeSinceLevelLoad - LastDown > HoldDuration)
        //{
        //    IsStuffEnabled = !IsStuffEnabled;
        //    Toggle(IsStuffEnabled);
        //    LastDown = float.MaxValue;
        //}

        if(OVRInput.GetDown(OVRInput.Button.Start, OVRInput.Controller.All))
        {
            IsStuffEnabled = !IsStuffEnabled;
            Toggle(IsStuffEnabled);
            LastDown = float.MaxValue;
        }
    }
}
